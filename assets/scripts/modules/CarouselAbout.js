import { module } from 'modujs';


export default class extends module {
    constructor(m) {
        super(m);
        this.carousel = this.$('carousel')[0]
        if(!this.carousel) this.carousel = this.el

        this.arrowNext = this.$('next')[0]
        this.arrowPrev = this.$('prev')[0]
    }

    init() {
        this.length = this.$('item').length

        const args = {
            speed: 600,
            loop: false,
            spaceBetween: 10,
            grabCursor: true,
            slidesPerView: 1,
            threshold: 1,
            navigation: {
                nextEl: this.arrowNext,
                prevEl: this.arrowPrev,
            },
            breakpoints: {
                700: {
                  slidesPerView: 2,
                  spaceBetween: 20
                }
            }
        }

        if(this.length > 1) this.carousel = new Swiper(this.carousel, args)
    }

    destroy() {
        if(this.carousel && this.carousel.destroy) this.carousel.destroy(true, true)
    }
}
