import { module } from 'modujs';
import { lerp } from '../utils/Maths';
import { transform } from '../utils/transform';

export default class extends module {
    constructor(m) {
        super(m);
    }

    init() {

        if(window.isMobile) return;

        this.hasMoved = false;

        this.mouse = {
            x: 0,
            y: 0,
            lerpedX: 0,
            lerpedY: 0
        }

        this.animate();

        this.bindMousemove = this.mousemove.bind(this);
        window.addEventListener('mousemove',this.bindMousemove);

        this.bindEnter = this.hover.bind(this);
        this.bindLeave = this.leave.bind(this);

        this.hoverElements = document.querySelectorAll('a, button, .js-hover, .js-hover-video, .js-hover-drag');
        this.hoverElements.forEach(el => {
            el.addEventListener('mouseenter', this.bindEnter);
            el.addEventListener('mouseleave', this.bindLeave);
        });
    }

    animate() {
        this.raf = requestAnimationFrame(() => this.animate());

        this.mouse.lerpedX = lerp(this.mouse.lerpedX, this.mouse.x, 0.1);
        this.mouse.lerpedY = lerp(this.mouse.lerpedY, this.mouse.y, 0.1);

        transform(this.el,`translate3d(${this.mouse.lerpedX}px,${this.mouse.lerpedY}px,0)`)
    }

    mousemove(e) {
        if(!this.hasMoved) {
            this.hasMoved = true;
            this.el.classList.add('has-moved');
        }

        this.mouse.x = e.clientX;
        this.mouse.y = e.clientY;
    }

    hover(e) {
        if(e.currentTarget.classList.contains('js-hover-video')) {
            this.el.classList.add('has-hover-video');
        }
        if(e.currentTarget.classList.contains('js-hover-drag')) {
            this.el.classList.add('has-hover-drag');
        }

        this.el.classList.add('has-hover');
    }

    leave(e) {
        if(e.currentTarget.classList.contains('js-hover-video')) {
            this.el.classList.remove('has-hover-video');
        }
        if(e.currentTarget.classList.contains('js-hover-drag')) {
            this.el.classList.remove('has-hover-drag');
        }

        this.el.classList.remove('has-hover');
    }

    destroy() {
        if(window.isMobile) return;

        window.removeEventListener('mousemove',this.bindmousemove);

        this.hoverElements.forEach(el => {
            el.removeEventListener('mouseenter', this.bindEnter);
            el.removeEventListener('mouseleave', this.bindLeave);
        });

        cancelAnimationFrame(this.raf);

    }
}
