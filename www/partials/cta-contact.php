<a href="#contact" data-scroll-to class="c-button -line || u-pointer-auto">
    <span class="c-button_label">Talk to a design advisor</span>
    <span class="c-button_icon">
        <svg class="c-button_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
        <svg class="c-button_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
    </span>
</a>