
            <div class="c-background-video">
                <div class="c-background-video_wrapper">
                    <video preload="auto" autoplay muted playsinline loop>
                        <source src="assets/videos/background.mp4" type="video/mp4">
                    </video>
                </div>
            </div>

            <div class="c-video-modal" data-module-video-modal>
                <button type="button" class="c-button -line || c-video-modal_close" data-video-modal="close">
                    <span class="c-button_label">Close</span>
                </button>

                <div class="c-video-modal_content">
                    <div class="c-video-modal_inner" data-video-modal="inner"></div>
                </div>
            </div>
        </div>

        <script nomodule src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.6.0/polyfill.min.js" crossorigin="anonymous"></script>
        <script nomodule src="https://polyfill.io/v3/polyfill.min.js?features=fetch%2CCustomEvent%2CElement.prototype.matches%2CNodeList.prototype.forEach%2CAbortController%2CElement.prototype.append" crossorigin="anonymous"></script>

        <script src="assets/scripts/vendors.js" defer></script>
        <script src="assets/scripts/app.js" defer></script>
    </body>
</html>
