<!doctype html>
<html class="is-loading" lang="en" data-page="home">
    <head>
        <meta charset="utf-8">
        <title>MinamiDesignCo.</title>
        <meta name="description" content="We know how hard it is to win in a crowded marketplace, that's why we have the right Design plan to make your business thrive. Talk to a Design advisor today.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <style media="all">
            <?php echo file_get_contents('assets/styles/critical.css'); ?>
        </style>

        <meta property="og:type"        content="website">
        <meta property="og:url"         content="http://<?php echo $_SERVER['HTTP_HOST']?><?php echo $_SERVER['REQUEST_URI'];?>"/>
        <meta property="og:image"       content="http://<?php echo $_SERVER['HTTP_HOST']?>/assets/images/og-image.png"/>
        <meta property="og:site_name"   content="MinamiDesignCo">
        <meta property="og:title"       content="MinamiDesignCo">
        <meta property="og:description" content="We know how hard it is to win in a crowded marketplace, that's why we have the right Design plan to make your business thrive. Talk to a Design advisor today.">

        <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <link id="stylesheet" rel="stylesheet" href="assets/styles/main.css?v=1.0" media="print" onload="this.media='all'; this.onload=null; this.isLoaded=true">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2755646-13"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-2755646-13');
        </script>
    </head>
    <body data-module-load>
        <div class="c-intro">
            <div class="c-intro_logo" data-module-intro></div>
        </div>
        <div data-load-container class="c-page">
            <header class="c-header">
                <div class="o-container">
                    <div class="c-header_container">
                        <a href="https://minami.digital/" title="MinamiDesignCo." class="c-header_logo" target="_blank" rel="noopener">
                            <svg role="img"><use xlink:href="assets/images/sprite.svg#logo"></use></svg>
                        </a>
                        <div data-module-nav>
                            <button type="button" href="#contact" class="c-button || c-header_button" data-nav="link">
                                <span class="c-button_label || c-header_button_label">Start a project</span>
                                <span class="c-button_icon || c-header_button_icon">
                                    <svg class="c-button_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                    <svg class="c-button_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </header>

            <nav class="c-nav" data-module-nav>
                <div class="c-nav_button"></div>
                <ul class="c-nav_list">
                    <li class="c-nav_item">
                        <button type="button" href="#hero" class="c-nav_link" data-nav="link">
                            <span class="c-nav_label">Start</span>
                            <span class="c-nav_dot"></span>
                        </button>
                    </li>
                    <li class="c-nav_item">
                        <button type="button" href="#block-1" class="c-nav_link" data-nav="link">
                            <span class="c-nav_label">The Problem</span>
                            <span class="c-nav_dot"></span>
                        </button>
                    </li>
                    <li class="c-nav_item">
                        <button type="button" href="#block-2" class="c-nav_link" data-nav="link">
                            <span class="c-nav_label">The Solution</span>
                            <span class="c-nav_dot"></span>
                        </button>
                    </li>
                    <li class="c-nav_item">
                        <button type="button" href="#block-4" class="c-nav_link" data-nav="link">
                            <span class="c-nav_label">Why Us</span>
                            <span class="c-nav_dot"></span>
                        </button>
                    </li>
                    <li class="c-nav_item">
                        <button type="button" href="#block-family" class="c-nav_link" data-nav="link">
                            <span class="c-nav_label">What we do</span>
                            <span class="c-nav_dot"></span>
                        </button>
                    </li>
                    <li class="c-nav_item">
                        <button type="button" href="#block-how" class="c-nav_link" data-nav="link">
                            <span class="c-nav_label">The Plan</span>
                            <span class="c-nav_dot"></span>
                        </button>
                    </li>
                    <li class="c-nav_item">
                        <button type="button" href="#block-checklist" class="c-nav_link" data-nav="link">
                            <span class="c-nav_label">Quiz</span>
                            <span class="c-nav_dot"></span>
                        </button>
                    </li>
                    <li class="c-nav_item">
                        <button type="button" href="#contact" class="c-nav_link" data-nav="link">
                            <span class="c-nav_label">Get in touch</span>
                            <span class="c-nav_dot"></span>
                        </button>
                    </li>
                </ul>
            </nav>

            <div class="c-cursor">
                <div class="c-cursor_inner" data-module-cursor>
                    <div class="c-cursor_dot"></div>
                    <div class="c-cursor_drag">
                        <svg class="c-cursor_drag_arrow -left" role="img"><use xlink:href="assets/images/sprite.svg#arrow-left-small"></use></svg>
                        <svg class="c-cursor_drag_arrow -right" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right-small"></use></svg>
                    </div>
                    <div class="c-cursor_play">
                        <span class="c-cursor_play_label">Play</span>
                    </div>
                </div>
            </div>